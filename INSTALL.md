# Light SDK Installation Guide


Here are the steps to use the light SDK.

## Build Installation

### Add Light SDK package in your root project

First download [Android light SDK][1] from the developer resources page,
then put it inside your root project and unzip it.

Then add inside "settings.gradle":
``` java
'your_project' , 'ONprint_Light'
```

SDK Light target version is actually running with API lvl 33.


###  Gradle intitialization

Then add inside your main build.gradle:

``` java
dependencies {
   classpath 'com.android.tools.build:gradle:8.1.1'
   classpath 'io.realm:realm-gradle-plugin:10.15.1'
}
```

Add inside your application/build.gradle:
``` java
buildscript {
    repositories {
        mavenCentral()
        google()
    }

    dependencies {
        classpath 'com.android.tools.build:gradle:4.2.2'
    }
}

apply plugin: 'com.android.application'
apply plugin: 'realm-android'

repositories {
    mavenCentral()
    google()
}

dependencies {
    implementation 'io.realm:realm-gradle-plugin:10.15.1'
    implementation 'com.squareup.okhttp3:okhttp:3.14.9'
    implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
    implementation 'org.apache.commons:commons-text:1.10.0'
    implementation project(':ONprint_Light')
}

android {
namespace 'YOUR_PACKAGE_NAME'

    defaultConfig {
        compileSdk = 33
        ...
    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_11
        targetCompatibility JavaVersion.VERSION_11
    }
    ...
}
```

### Manifest permissions

Add inside the AndroidManifest.xml the following permissions:
``` java
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.INTERNET"/>
```

## Java Initialization

### Data Base initialization
``` java
Realm.init(getApplicationContext());
RealmConfiguration config = new RealmConfiguration.Builder()
    .name("database.realm")
    .schemaVersion(1)
    .deleteRealmIfMigrationNeeded()
    .build();
Realm.setDefaultConfiguration(config);

```

### SDK intialization

To init ONprint SDK you need a apiKey which is a character chain allowing you to use ONprint API services with your dedicating data.

``` java
ONprint.init(context, apiKey);
```

To obtain your personal API Key, please contact mobile@onprint.com


## Dev

Now it's time... to code!
Please follow -> the [developer guide][2]

TIP: when you're performing an SDK upgrade,
first update your ONprint_Light/sdk_light.aar file,
and then please have a look at the upgrade commit; it can be inspiring!


[1]: http://developer.onprint.com/sdk.html
[2]: https://gitlab.com/onprint_public/Android_ONprint_Light_SDK_Sample/tree/master/DEV.md
