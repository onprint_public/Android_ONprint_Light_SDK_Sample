# Developer Guide

Welcome to the ONprint developer guide.<br/><br/>
This document lists all the main ONprint light SDK functions to explain you how
to build an Android application using ONprint picture recognition.

## Init APIpref

Here is APIpref signature:
``` java
public APIpref(String applicationID, String applicationName, String applicationVersion, String apiKey)
```

The following code shows you how you can simply initialize APIpref:
``` java
String app_name = getResources().getString(R.string.app_name);
String sample_version = "?";
int sample_version_code = 0;
try {
    PackageInfo sample_info = Objects.requireNonNull(getContext()).getPackageManager()
        .getPackageInfo(getContext().getPackageName(), 0);
    sample_version_code = sample_info.versionCode;
    sample_version = sample_info.versionName;
} catch (PackageManager.NameNotFoundException e) {
    e.printStackTrace();
}

APIpref prefs = new APIpref(getApplicationInstanceId(getContext()),
    app_name + "_" + sample_version_code,
    sample_version,
    getApiKey(getContext()));
```

## Init request variable

``` java
DeviceInformation di = new DeviceInformation(getContext());
String system_language = di.getSystemLanguage();
String query = "{\"Language\":\"" + system_language + "\"}";
RequestBody requestQuery = RequestBody.create(MediaType.parse("application/json"), query);
HashMap<String, String> headers = new HashMap<>();
```

## Create the body request from a JPEG File

``` java
Bitmap bitmap = BitmapFactory.decodeFile(mFile.getAbsolutePath());
ByteArrayOutputStream blob = new ByteArrayOutputStream();
bitmap.compress(Bitmap.CompressFormat.JPEG, 60, blob);
bmp_picture = new ONprintBMP(getActivity(), blob.toByteArray());

RequestBody requestFile = get_request_body(bmp_picture);

MultipartBody.Part bodyFile =
MultipartBody.Part.createFormData("file", bmp_picture.get_pic().getName(), requestFile);
```

Note: don't forget to compress the picture before sending it!

You can can also call this constructor to create ONprint BMP image:
``` java
public ONprintBMP(Activity activity, File file)
```

## Set ONprint Picture mode service "call" with HTTPS Picture informations



ONprintAPI object is used to make ONprint requests. Below its initialization:

``` java
ONprintAPI onprintAPI_secured = ServiceGenerator.createService(getContext(), prefs, ONprintAPI.class, true);
```

### > getPicture API function

This function is called with a numeric picture as parameter to get its associated Enriched Image objects.
Returned objects will be ordered by probability (score) detection.
It is also possible to pass a keywords list to filter the result.

``` java
JSONArray keywords = new JSONArray();
//keywords.put("tata"); // Keywords is a list of filter words
String query_pic = "{\"Keywords\":" + keywords + "}";
RequestBody requestQuery_pic = RequestBody.create(MediaType.parse("application/json"), query_pic);
call = onprintAPI_secured.getPicture(headers, bodyFile, requestQuery_pic);
```

### > onprintAPI_secured.getPictureById API function

This function is called with an Picture ID as parameter to get its associated Picture object.

``` java
HashMap<String, String> map = new HashMap<>();
map.put("id", picture_id);
call = onprintAPI_secured.getPictureById(headers, map);
```



## Set ONprint Enriched mode service "call" to get full enriched informations

ONprintAPI object is used to make ONprint requests. Below its initialization:

``` java
ONprintAPI onprintAPI = ServiceGenerator.createService(getContext(), prefs, ONprintAPI.class, false);
```

### > getEnrichedImage API function


This function is called with a numeric picture as parameter to get its associated Enriched Image object.

``` java
DeviceInformation di = new DeviceInformation(getContext());
String system_language = di.getSystemLanguage();
String query = "{\"Language\":\"" + system_language + "\"}";
RequestBody requestQuery = RequestBody.create(MediaType.parse("application/json"), query);
call = onprintAPI.getEnrichedImage(headers, bodyFile, requestQuery);
```

### > getEnrichedImageById API function

This function is called with an Enriched Image ID as parameter to get its associated Enriched Image object.

``` java
HashMap<String, String> map = new HashMap<>();
map.put("id", enriched_image_id);
map.put("languageCode", new DeviceInformation(getContext()).getSystemLanguage());
call = onprintAPI.getEnrichedImageById(headers, map);
```


### > sendClick API function

For each user session you can record user actions.
Statistics can be consult in the ONprint admin console.

``` java
call = onprintAPI.sendClick(sessionId, actionId);
```


## Define the main callback treatment

Import ONprint server return values to filter the server return:
``` java
import static com.onprint.ws.tools.APIvalue.API_KEY_NOT_AUTHORIZED;
import static com.onprint.ws.tools.APIvalue.ENRICHED_IMAGE_NOT_FOUND;
import static com.onprint.ws.tools.APIvalue.ONPRINT_OK;
import static com.onprint.ws.tools.APIvalue.PICTURE_NOT_FOUND;
```

API_KEY_NOT_AUTHORIZED is returned when the API key is unknown.<br/>
ENRICHED_IMAGE_NOT_FOUND and PICTURE_NOT_FOUND are returned when the requested image/picture is not connected.<br/>
ONPRINT_OK is returned when the image/picture is connected. The content is a JSON which is inside the body response.<br/>

The treatment will be done inside the call callback:

``` java
call.enqueue(new Callback<ResponseBody>() {
@Override
public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
    Log.i(TAG, "Code [" + response.code() + "] Message -> " + response.message());
    photo_available(true);
    JSONObject json;
    switch (response.code()) {

	// Treatment when image is recognized
	case ONPRINT_OK:
	    try {
		assert response.body() != null;

		if (picture_mode.isChecked() && !test_getById) {
		    // Treat minimal mode:
		    // the response is a list of ID sorted by recognition rate
		    JSONArray jsonArray = new JSONArray(response.body().string());

		    // Get the first element of the list,
		    // which is the element with the best recognition rate
		    json = jsonArray.getJSONObject(0);
		} else {
		    json = new JSONObject(response.body().string());
		}
		response.body().close();
		enrichedCallback.enriched(getContext(), json);
	    } catch (Exception e) {
            String error = "Exception: " + e.getMessage();
            Log.e(TAG, error);
            e.printStackTrace();
            showToast(error);
	    }
	    break;

	// When API key is not declared
	case API_KEY_NOT_AUTHORIZED:
	    enrichedCallback.failed("This application is not authorized "
		    + "to execute this request");
	    break;

	// Treatment when Enriched Image or Picture is NOT CONNECTED
	case ENRICHED_IMAGE_NOT_FOUND:
	case PICTURE_NOT_FOUND:
	    enrichedCallback.notEnriched();
	    break;

	// ERROR Cases
	default:
	    Log.e(TAG, "default error [" + response.code() + "]");
	    assert response.errorBody() != null;
	    try {
            enrichedCallback.failed("[Error "+ response.code() + "] "
                + response.errorBody().string());
	    } catch (Exception e) {
            Log.e(TAG, "default exception: " + e.getMessage());
            e.printStackTrace();
	    }
    }
}

@Override
public void onFailure(@NonNull Call<ResponseBody> call, Throwable throwable) {
    Log.e(TAG, "onFailure: " + throwable.getMessage());
    showToast("Scan fails: " + throwable.getMessage());
    photo_available(true);
}
});
```

## Define the callback's features

``` java
public void init_callback() {
enrichedCallback = new IEnrichedImage() {
    @Override
    public void enriched(Context context, JSONObject json_ONprint_object) {
	Log.i(TAG, "JSON: " + json_ONprint_object);

	if (id_mode.isChecked()) {
	    ONprintMinImage image = new ONprintMinImage(json_ONprint_object);
	    showToast("ONprint ID: " + image.getName());
	}
	else {
	    ONprintImage imageJson = new ONprintImage(json_ONprint_object);
	    showToast("Title: " + imageJson.getTitle());
	}
    }

    @Override
    public void notEnriched() {
	Log.e(TAG, "Image is not enriched");
	showToast("Image is not enriched");
    }

    @Override
    public void failed(String error) {
	Log.e(TAG, "IEnrichedImage error: " + error);
	showToast("External error");
    }
};
}
```

