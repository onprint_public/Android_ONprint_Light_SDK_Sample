# Android ONprint Light SDK Sample

In no time, create an application to get information of enriched images by taking picture.

## Introduction

This [Sample API][1] shows you how to use basic functionalities of ONprint light SDK.
By using [ONprint API][2], it provides you an interface for individual camera
connected to an Android device in order
to get informations from picture.
It also can record click stats from a webview into a dedicated server.

In this sample two modes are available:
- **Picture mode**: getting picture data associated to a picture.
- **Enriched Image mode**: getting more informations (title, actions, etc.) about an enriched image.

Picture recognition unfolds in three steps:
1. taking a picture
1. sending this picture to ONprint server
1. getting associated data if the picture is identified

Furthermore, it allows you to record usage stats.

## Pre-requisites

- Android SDK 21
- Compile SDK Version 28
- Android Support Repository
- Java 1.8


## Screenshots

<img src="screenshots/main.jpg" height="400" alt="Screenshot"/> 


## Getting Started

This sample uses the Gradle build system. To build this project, use the
"gradlew build" command or use "Import Project" in Android Studio.


## SDK Integration

To start, install the SDK for your own project by following the [install doc][4].
<br/><br/>
Then, to integrate ONprint functions, please consult the [developer guide][5].


### Support

If you've found an error in this sample or if you have any question,
please send us an email to: mobile@onprint.com


### License

© 2019 by LTU Tech. All Rights Reserved.

[1]: https://gitlab.com/onprint_public/Android_ONprint_Light_SDK_Sample
[2]: https://developer.android.com/reference/android/hardware/camera2/package-summary.html
[3]: http://developer.onprint.com/api.html
[4]: https://gitlab.com/onprint_public/Android_ONprint_Light_SDK_Sample/tree/master/INSTALL.md
[5]: https://gitlab.com/onprint_public/Android_ONprint_Light_SDK_Sample/tree/master/DEV.md

